
package hu.horvathbzs.logistics.factory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.horvathbzs.logistics.config.LogisticsConfigProperties;
import hu.horvathbzs.logistics.model.Address;
import hu.horvathbzs.logistics.model.Milestone;
import hu.horvathbzs.logistics.model.Section;
import hu.horvathbzs.logistics.model.TransportPlan;
import hu.horvathbzs.logistics.repository.AddressRepository;
import hu.horvathbzs.logistics.repository.MilestoneRepository;
import hu.horvathbzs.logistics.repository.SectionRepository;
import hu.horvathbzs.logistics.repository.TransportPlanRepository;

/**
 * Class contains methods that create and save test instances.
 * 
 * @author balazs.horvath
 *
 */
@Service
public class TestFactory {

	@Autowired
	LogisticsConfigProperties config;

	@Autowired
	AddressRepository addressRepository;

	@Autowired
	MilestoneRepository milestoneRepository;

	@Autowired
	SectionRepository sectionRepository;

	@Autowired
	TransportPlanRepository transportPlanRepository;

	/**
	 * Finds the first reduce value from the properties file.
	 * 
	 * @return Double e.g. 0.05
	 */
	public Double firstReduceValue() {
		TreeMap<Long, Double> limits = config.getExpectedRevenueReduceByDelay().getLimits();
		Double firstReduce = limits.get(limits.firstKey());
		return firstReduce;
	}

	/**
	 * Gets the limit value from the properties.
	 * 
	 * @return TreeMap<Long, Double> key: minutes, value reduce value
	 */
	public TreeMap<Long, Double> getLimits() {
		return config.getExpectedRevenueReduceByDelay().getLimits();
	}

	public void clearDb() {
//		milestoneRepository.deleteAll();
//		addressRepository.deleteAll();	
		sectionRepository.deleteAll();
		milestoneRepository.deleteAll();
		addressRepository.deleteAll();	
		transportPlanRepository.deleteAll();
	}

	public List<TransportPlan> initDb() {

		List<TransportPlan> transportPlans = createTransportPlans(createSections(createMilestones(createAddresses())));

		List<Section> sections = sectionRepository.findAll();

		for (int i = 0; i < 2; i++) {
			sections.get(i).setTransportPlan(transportPlans.get(0));
			sections.add(sections.get(i));
			// sectionRepository.save(sections.get(i));
		}

		sections.get(2).setTransportPlan(transportPlans.get(1));
		sections.add(sections.get(2));
		sectionRepository.saveAll(sections);

		return transportPlans;
	}
	
	public List<TransportPlan> getAllTransportPlansFull() {
		return transportPlanRepository.findAllFull();
	}

	public List<TransportPlan> createTransportPlans(List<Section> sections) {

		List<TransportPlan> transportPlans = new ArrayList<>();

		TransportPlan transportPlan1 = new TransportPlan();
		transportPlan1.setExpectedRevenue(1000.0);
		transportPlans.add(transportPlan1);

		TransportPlan transportPlan2 = new TransportPlan();
		transportPlan2.setExpectedRevenue(500.0);
		transportPlans.add(transportPlan2);

		return transportPlanRepository.saveAll(transportPlans);
	}

	public List<Section> createSections(List<Milestone> milestones) {

		List<Section> sections = new ArrayList<>();

		int from = 0;
		int to = 1;
		for (int i = 0; i < 2; i++) {
			Section section = new Section();
			section.setSectionIndex(i);
			section.setFromMilestone(milestones.get(from));
			section.setToMilestone(milestones.get(to));
			from = from + 2;
			to = to + 2;
			sections.add(section);
		}

		Section section = new Section();
		section.setSectionIndex(0);
		section.setFromMilestone(milestones.get(from));
		section.setToMilestone(milestones.get(to));
		sections.add(section);

		return sectionRepository.saveAll(sections);
	}

	public List<Milestone> createMilestones(List<Address> addresses) {

		List<Milestone> milestones = new ArrayList<>();

		LocalDateTime dt1 = LocalDateTime.of(2021, 5, 10, 10, 0);
		LocalDateTime dt2 = LocalDateTime.of(2021, 5, 10, 11, 0);
		LocalDateTime dt3 = LocalDateTime.of(2021, 5, 10, 11, 30);
		LocalDateTime dt4 = LocalDateTime.of(2021, 5, 10, 11, 45);
		LocalDateTime dt5 = LocalDateTime.of(2021, 5, 10, 12, 0);
		LocalDateTime dt6 = LocalDateTime.of(2021, 5, 10, 13, 0);

		Milestone m1 = new Milestone();
		m1.setAddress(addresses.get(0));
		m1.setPlannedTime(dt1);
		milestones.add(m1);

		Milestone m2 = new Milestone();
		m2.setAddress(addresses.get(1));
		m2.setPlannedTime(dt2);
		milestones.add(m2);

		Milestone m3 = new Milestone();
		m3.setAddress(addresses.get(1));
		m3.setPlannedTime(dt3);
		milestones.add(m3);

		Milestone m4 = new Milestone();
		m4.setAddress(addresses.get(2));
		m4.setPlannedTime(dt4);
		milestones.add(m4);

		Milestone m5 = new Milestone();
		m5.setAddress(addresses.get(2));
		m5.setPlannedTime(dt5);
		milestones.add(m5);

		Milestone m6 = new Milestone();
		m6.setAddress(addresses.get(3));
		m6.setPlannedTime(dt6);
		milestones.add(m6);

		return milestoneRepository.saveAll(milestones);
	}

	public List<Address> createAddresses() {
		List<Address> addresses = new ArrayList<>();

		for (int i = 0; i < 4; i++) {
			Address address = new Address();
			address.setCountry2DigitIsoCode("AA");
			address.setMunicipality("Test Municipality");
			address.setStreet(i + " Test Street");
			address.setHouseNumber("" + 5 + i);
			address.setZipCode("H-8000");
			addresses.add(address);
		}
		return addressRepository.saveAll(addresses);
	}

}
