package hu.horvathbzs.logistics.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

import java.time.temporal.ChronoUnit;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.reactive.server.WebTestClient.ResponseSpec;

import hu.horvathbzs.logistics.dto.LoginDto;
import hu.horvathbzs.logistics.dto.TransportPlanDelayDto;
import hu.horvathbzs.logistics.dto.TransportPlanDto;
import hu.horvathbzs.logistics.factory.TestFactory;
import hu.horvathbzs.logistics.repository.MilestoneRepository;

/**
 * Integration test for TransportPlan web tier.
 * 
 * @author balazs.horvath
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
public class TransportPlanControllerIT {

	private static final String BASE_URI = "api/transportPlans";

	@Autowired
	WebTestClient webTestClient;

	@Autowired
	TestFactory testFactory;

	@Autowired
	MilestoneRepository milestoneRepository;

	private String jwtToken;

	@BeforeEach
	public void init() {
		testFactory.clearDb();

		LoginDto body = new LoginDto();
		body.setUsername("userTransportManager");
		body.setPassword("pass");
		jwtToken = webTestClient.post().uri("/api/login").bodyValue(body).exchange().expectBody(String.class)
				.returnResult().getResponseBody();
	}

	@Test
	void testThatTransportPlanNotFound() throws Exception {

		/**
		 * create dummy transportDelayDto without saving to database
		 */
		Long delayMinutes = 30L;
		TransportPlanDelayDto delayDto = new TransportPlanDelayDto(/* milestoneId */2L, delayMinutes);

		/**
		 * assert that status is NOT FOUND
		 */
		assertThat(delayWithNonExistingTransportPlan(-1L, delayDto).getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
	}

	private EntityExchangeResult delayWithNonExistingTransportPlan(Long transportPlanId, TransportPlanDelayDto delayDto) {
		return webTestClient.post().uri(BASE_URI + "/" + transportPlanId + "/delay")
				.headers(headers -> headers.setBearerAuth(jwtToken)).bodyValue(delayDto).exchange().expectStatus()
				.isNotFound().expectBody(TransportPlanDto.class).returnResult();
	}

	@Test
	void testThatMilestoneIdNotInAnyTransportPlanSections() throws Exception {

		Long delayMinutes = 30L;

		/**
		 * init data and create transportDelayDto
		 */
		testFactory.initDb();
		List<TransportPlanDto> transportPlans = getAllTransportPlans();
		TransportPlanDto transportPlanDtoBefore = transportPlans.get(0);
		Long transportPlanId = transportPlanDtoBefore.getId();
		Long nextTransportPlanMilestoneId = transportPlans.get(1).getSections().get(0).getFromMilestoneId();
		TransportPlanDelayDto delayDto = new TransportPlanDelayDto(nextTransportPlanMilestoneId, delayMinutes);

		/**
		 * call the tested endpoint, then query the TransportPlan again
		 */
		delayWithMilestoneIdThatNotInAnyTransportPlanSections(transportPlanId, delayDto);
		TransportPlanDto transportPlanDtoAfter = getTransportPlanById(transportPlanId);

		/**
		 * assert that TransportPlan expectedRevenue has NOT been modified
		 */
		assertThat(transportPlanDtoBefore.getExpectedRevenue()).isEqualTo(transportPlanDtoAfter.getExpectedRevenue());

	}

	private ResponseSpec delayWithMilestoneIdThatNotInAnyTransportPlanSections(Long transportPlanId,
			TransportPlanDelayDto delayDto) {
		return webTestClient.post().uri(BASE_URI + "/" + transportPlanId + "/delay")
				.headers(headers -> headers.setBearerAuth(jwtToken)).bodyValue(delayDto).exchange().expectStatus()
				.isBadRequest();
	}

	@Test
	void testThatDelayIsRegisteredOnTransportPlan() throws Exception {

		Long delayMinutes = testFactory.getLimits().firstKey();
		Double reduceValue = testFactory.getLimits().get(delayMinutes);

		/**
		 * init data and create transportDelayDto
		 */
		testFactory.initDb();
		List<TransportPlanDto> transportPlans = getAllTransportPlans();
		TransportPlanDto transportPlanDtoBefore = transportPlans.get(0);
		Long transportPlanId = transportPlanDtoBefore.getId();
		Double expectedRevenueBeforeDelay = transportPlanDtoBefore.getExpectedRevenue();
		Long milestoneId = transportPlanDtoBefore.getSections().get(0).getFromMilestoneId();
		TransportPlanDelayDto delayDto = new TransportPlanDelayDto(milestoneId, delayMinutes);

		/**
		 * call the tested endpoint, then query the TransportPlan again
		 */
		delayOk(transportPlanId, delayDto);
		TransportPlanDto transportPlanDtoAfter = getTransportPlanById(transportPlanId);
		Double expectedRevenueAfterDelay = transportPlanDtoAfter.getExpectedRevenue();

		/**
		 * assert that TransportPlan expectedRevenue has been modified correctly
		 */
		assertThat(expectedRevenueAfterDelay).isEqualTo(expectedRevenueBeforeDelay * (1 - reduceValue));
	}

	private TransportPlanDto delayOk(Long transportPlanId, TransportPlanDelayDto delayDto) {
		return webTestClient.post().uri(BASE_URI + "/" + transportPlanId + "/delay")
				.headers(headers -> headers.setBearerAuth(jwtToken)).bodyValue(delayDto).exchange().expectStatus()
				.isOk().expectBody(TransportPlanDto.class).returnResult().getResponseBody();
	}

	/**
	 * Get list of transport plans by calling web layer endpoint.
	 * 
	 * @return List<TransportPlanDto>
	 */
	private List<TransportPlanDto> getAllTransportPlans() {
		return webTestClient.get().uri(BASE_URI).exchange().expectStatus().isOk().expectBodyList(TransportPlanDto.class)
				.returnResult().getResponseBody();
	}

	/**
	 * Get a transport plan by calling web layer endpoint.
	 * 
	 * @param id tranportPlanId
	 * @return TransportPlanDto
	 */
	private TransportPlanDto getTransportPlanById(Long id) {
		return webTestClient.get().uri(BASE_URI + "/" + id).exchange().expectStatus().isOk()
				.expectBody(TransportPlanDto.class).returnResult().getResponseBody();
	}
}
