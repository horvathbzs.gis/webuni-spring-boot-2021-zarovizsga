
package hu.horvathbzs.logistics.service;

import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import hu.horvathbzs.logistics.factory.TestFactory;
import hu.horvathbzs.logistics.model.Milestone;
import hu.horvathbzs.logistics.model.TransportPlan;
import hu.horvathbzs.logistics.repository.MilestoneRepository;

/**
 * @author balazs.horvath
 *
 */

@SpringBootTest
@AutoConfigureTestDatabase
public class TransportPlanServiceIT {

	@Autowired
	TestFactory testFactory;

	@Autowired
	MilestoneRepository milestoneRepository;

	@Autowired
	TransportPlanService transportPlanService;

	@BeforeEach
	public void init() {
		testFactory.clearDb();
	}

	@Test
	void testDelayWithFromMilestoneId() throws Exception {

		Long delayMinutes = testFactory.getLimits().firstKey();
		Double reduceValue = testFactory.getLimits().get(delayMinutes);

		/**
		 * init data and create transportDelayDto
		 */
		testFactory.initDb();
		List<TransportPlan> transportPlans = transportPlanService.findAllFull();
		TransportPlan transportPlanBeforeDelay = transportPlans.get(0);
		Long transportPlanId = transportPlanBeforeDelay.getId();
		Double expectedRevenueBeforeDelay = transportPlanBeforeDelay.getExpectedRevenue();
		Long fromMilestoneId = transportPlanBeforeDelay.getSections().get(0).getFromMilestone().getId();
		Long toMilestoneId = transportPlanBeforeDelay.getSections().get(0).getToMilestone().getId();
		Milestone fromMilestoneBeforeDelay = milestoneRepository.findById(fromMilestoneId).get();
		Milestone toMilestoneBeforeDelay = milestoneRepository.findById(toMilestoneId).get();

		/**
		 * call the tested method with a fromMilestoneId
		 */
		TransportPlan transportPlanAfterDelay = transportPlanService.registerDelay(transportPlanId, fromMilestoneId,
				delayMinutes);
		Double expectedRevenueAfterDelay = transportPlanAfterDelay.getExpectedRevenue();
		Milestone fromMilestoneAfterDelay = milestoneRepository.findById(fromMilestoneId).get();
		Milestone toMilestoneAfterDelay = milestoneRepository.findById(toMilestoneId).get();

		/**
		 * assert that TransportPlan expectedRevenue, and fromMilestone.plannedTime and
		 * toMilestone.plannedTime has been modified correctly
		 */
		assertThat(expectedRevenueAfterDelay).isEqualTo(expectedRevenueBeforeDelay * (1 - reduceValue));
		assertThat(fromMilestoneAfterDelay.getPlannedTime()).isCloseTo(
				fromMilestoneBeforeDelay.getPlannedTime().plusMinutes(delayMinutes), within(1, ChronoUnit.MICROS));
		assertThat(toMilestoneAfterDelay.getPlannedTime()).isCloseTo(
				toMilestoneBeforeDelay.getPlannedTime().plusMinutes(delayMinutes), within(1, ChronoUnit.MICROS));
	}

	@Test
	void testDelayWithToMilestoneId() throws Exception {

		Long delayMinutes = testFactory.getLimits().firstKey();
		Double reduceValue = testFactory.getLimits().get(delayMinutes);

		/**
		 * init data and create transportDelayDto
		 */
		testFactory.initDb();
		List<TransportPlan> transportPlans = transportPlanService.findAllFull();
		TransportPlan transportPlanBeforeDelay = transportPlans.get(0);
		Long transportPlanId = transportPlanBeforeDelay.getId();
		Double expectedRevenueBeforeDelay = transportPlanBeforeDelay.getExpectedRevenue();
		Long toMilestoneId = transportPlanBeforeDelay.getSections().get(0).getToMilestone().getId();
		Long nextSectionFromMilestoneId = transportPlanBeforeDelay.getSections().get(1).getFromMilestone().getId();
		Milestone toMilestoneBeforeDelay = milestoneRepository.findById(toMilestoneId).get();
		Milestone nextSectionFromMilestoneBeforeDelay = milestoneRepository.findById(nextSectionFromMilestoneId).get();

		/**
		 * call the tested method with a toMilestoneId
		 */
		TransportPlan transportPlanAfterDelay = transportPlanService.registerDelay(transportPlanId, toMilestoneId,
				delayMinutes);
		Double expectedRevenueAfterDelay = transportPlanAfterDelay.getExpectedRevenue();
		Milestone toMilestoneAfterDelay = milestoneRepository.findById(toMilestoneId).get();
		Milestone nextSecttionFromMilestoneAfterDelay = milestoneRepository.findById(nextSectionFromMilestoneId).get();

		/**
		 * assert that TransportPlan expectedRevenue, and toMilestone.plannedTime and
		 * the following sections fromMilestone.plannedTime has been modified correctly
		 */
		assertThat(expectedRevenueAfterDelay).isEqualTo(expectedRevenueBeforeDelay * (1 - reduceValue));
		assertThat(toMilestoneAfterDelay.getPlannedTime()).isCloseTo(
				toMilestoneBeforeDelay.getPlannedTime().plusMinutes(delayMinutes), within(1, ChronoUnit.MICROS));
		assertThat(nextSecttionFromMilestoneAfterDelay.getPlannedTime()).isCloseTo(
				nextSectionFromMilestoneBeforeDelay.getPlannedTime().plusMinutes(delayMinutes),
				within(1, ChronoUnit.MICROS));
	}

	@Test
	void testDelayWithNonExistingTransportPlanId() throws Exception {

		/**
		 * call the tested method with a non-existing transportPlanId
		 */
		String exceptionMessage;
		try {
			TransportPlan transportPlanAfterDelay = transportPlanService.registerDelay(500L, 500L, 30L);
			exceptionMessage = "";
		} catch (Exception ex) {
			exceptionMessage = ex.getMessage();
		}

		/**
		 * assert that method throw exception with HttpStatus NOT FOUND
		 */
		assertThat(exceptionMessage).contains(HttpStatus.NOT_FOUND.toString());
	}

	@Test
	void testDelayWithNonExistingMilestoneId() throws Exception {

		/**
		 * init data and create transportDelayDto
		 */
		Long transportPlanId = testFactory.initDb().get(0).getId();

		/**
		 * call the tested method with a non-existing transportPlanId
		 */
		String exceptionMessage;
		try {
			TransportPlan transportPlanAfterDelay = transportPlanService.registerDelay(transportPlanId, 500L, 30L);
			exceptionMessage = "";
		} catch (Exception ex) {
			exceptionMessage = ex.getMessage();
		}

		/**
		 * assert that method throw exception with HttpStatus NOT FOUND
		 */
		assertThat(exceptionMessage).contains(HttpStatus.NOT_FOUND.toString());
	}

	@Test
	void testDelayWithMilestoneIdThatNotInAnyTransportPlanSections() throws Exception {

		Long delayMinutes = testFactory.getLimits().firstKey();
		Double reduceValue = testFactory.getLimits().get(delayMinutes);

		/**
		 * init data and create transportDelayDto
		 */
		testFactory.initDb();
		List<TransportPlan> transportPlans = transportPlanService.findAllFull();
		TransportPlan transportPlan = transportPlans.get(0);
		TransportPlan nextTansportPlan = transportPlans.get(1);
		Long transportPlanId = transportPlan.getId();
		Long milestoneId = nextTansportPlan.getSections().get(0).getToMilestone().getId();
		Milestone milestoneBeforeDelay = milestoneRepository.findById(milestoneId).get();

		/**
		 * call the tested method with a toMilestoneId
		 */
		String exceptionMessage;
		try {
			TransportPlan transportPlanAfterDelay = transportPlanService.registerDelay(transportPlanId, milestoneId,
					delayMinutes);
			exceptionMessage = "";
		} catch (Exception ex) {
			exceptionMessage = ex.getMessage();
		}

		/**
		 * assert that method throw exception with HttpStatus BAD REQUEST
		 */
		assertThat(exceptionMessage).contains(HttpStatus.BAD_REQUEST.toString());
	}
}
