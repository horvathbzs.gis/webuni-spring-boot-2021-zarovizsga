package hu.horvathbzs.logistics.service;

import java.util.List;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import hu.horvathbzs.logistics.config.LogisticsConfigProperties;
import hu.horvathbzs.logistics.model.Milestone;
import hu.horvathbzs.logistics.model.Section;
import hu.horvathbzs.logistics.model.TransportPlan;
import hu.horvathbzs.logistics.repository.MilestoneRepository;
import hu.horvathbzs.logistics.repository.SectionRepository;
import hu.horvathbzs.logistics.repository.TransportPlanRepository;

/**
 * Service class for Transport Plan.<br />
 * CRUD methods.<br />
 * Method for register a delay of a Transport Plan.
 * 
 * @author balazs.horvath
 *
 */

@Service
public class TransportPlanService {

	@Autowired
	TransportPlanRepository transportPlanRepository;

	@Autowired
	SectionRepository sectionRepository;

	@Autowired
	MilestoneRepository mileStoneRepository;

	@Autowired
	LogisticsConfigProperties config;

	/**
	 * Queries all of the Transport Plans.
	 * 
	 * @return List<TransportPlan> all transportPlans with sections.
	 */
	public List<TransportPlan> findAllFull() {
		// return transportPlanRepository.findAll();

		return transportPlanRepository.findAllFull();
	}

	/**
	 * Queries one Transport Plan.
	 * 
	 * @return Optional<TransportPlan> if given id exists, return with one Transport
	 *         Plan.
	 */
	public Optional<TransportPlan> findById(Long id) {
		return transportPlanRepository.findWithSectionsById(id);
	}

	/**
	 * Creates a Transport Plan record in the database.
	 * 
	 * @param transportPlan
	 * @return saved transportPlan with generated id.
	 */
	public TransportPlan save(TransportPlan transportPlan) {
		return transportPlanRepository.save(transportPlan);
	}

	/**
	 * Updates an existing Transport Plan record.
	 * 
	 * @param transportPlan
	 * @return updated transportPlan with existing id.
	 */
	@Transactional
	public TransportPlan update(TransportPlan transportPlan) {
		if (!transportPlanRepository.existsById(transportPlan.getId()))
			return null;
		return transportPlanRepository.save(transportPlan);
	}

	/**
	 * Deletes a transportPlan record.
	 * 
	 * @param id
	 */
	public void delete(Long id) {
		transportPlanRepository.deleteById(id);
	}

	/**
	 * Method checks whether the transport plan or the milestone with the id given
	 * in the parameter exists.<br />
	 * Finds the milestone's section.<br />
	 * If the milestone is a fromMilestone, then
	 * section.setFromAndToMilestonePlannedTimeWithDelay(Long delay) method is used
	 * to set the plannedTime on each milestone (from and to).<br />
	 * It the milestone is a toMilestone then the plannedTime is set with the delay,
	 * and the plannedTime is set on the following section's fromMilestone, if
	 * section exists.<br />
	 * The transport plan's expectedRevenue is reduced with a certain percent, that
	 * comes from configuration, application.yml file.
	 * 
	 * @param transportPlanId the transport plan where the delay is registered.
	 * @param milestoneId     the milestone where the delay is registered.
	 * @param delay           minutes added to milestone.plannedTime
	 * @return TransportPlan with updated expectedRevenue and updated plannedTime of
	 *         milestones.
	 */
	@Transactional
	public TransportPlan registerDelay(Long transportPlanId, Long milestoneId, Long delay) {

		TransportPlan transportPlan = transportPlanRepository.findById(transportPlanId)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

		Milestone milestone = mileStoneRepository.findById(milestoneId)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

		/**
		 * Find a section that has a fromMilestone or a toMilestone with the same id as
		 * the parameter milestoneId.
		 */
		Section section = transportPlan.getSections().stream()
				.filter(sect -> milestone.getId().equals(sect.getFromMilestone().getId())
						|| milestone.getId().equals(sect.getToMilestone().getId()))
				.findAny().orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));

		/**
		 * Parameter milestoneId is a section's fromMilestone's id then adds delay
		 * minutes to toMilestone's planned time as well.
		 */
		if (milestoneId.equals(section.getFromMilestone().getId())) {
			section.modifyFromAndToMilestonePlannedTimeWithDelay(delay);
		}

		/**
		 * Parameter milestoneId is a section's toMilestone's id then adds delay minutes
		 * the next section's fromMilestone's planned time if next section exists.
		 */
		if (milestoneId.equals(section.getToMilestone().getId())) {
			Milestone toMilestone = section.getToMilestone();
			toMilestone.setPlannedTime(toMilestone.getPlannedTime().plusMinutes(delay));

			Optional<Section> nextSection = sectionRepository.findByTransportPlanAndSectionIndex(transportPlan,
					section.getSectionIndex() + 1);
			if (!nextSection.isEmpty()) {
				System.out.println("TransportPlanService > registerDelay() > nextSection.index: "
						+ nextSection.get().getSectionIndex());
				Milestone nextSectionFromMilestone = nextSection.get().getFromMilestone();
				System.out.println("TransportPlanService > registerDelay() > nextSectionFromMilestone.id: "
						+ nextSectionFromMilestone.getId());
				nextSectionFromMilestone.setPlannedTime(nextSectionFromMilestone.getPlannedTime().plusMinutes(delay));
			}

		}

		/**
		 * Get limits from config. Limits are the properties for minutes of delay (key)
		 * and reduce percent of expected revenue (value).
		 */
		TreeMap<Long, Double> limits = config.getExpectedRevenueReduceByDelay().getLimits();

		/**
		 * The expected revenue reduce percent is given in the application.yml
		 * file.<br />
		 * The floorEntry method finds the nearest lower key from the TreeMap that
		 * matches the parameter delay. The transportPlan.expectedRevenue is updated
		 * with percent reduce.
		 */
		Double revenueReducePercent;
		Entry<Long, Double> floorEntry = limits.floorEntry(delay);
		revenueReducePercent = floorEntry == null ? 0 : floorEntry.getValue();

		Double expectedRevenue = transportPlan.getExpectedRevenue();
		transportPlan.setExpectedRevenue(expectedRevenue * (1.0 - revenueReducePercent));

		return transportPlan;
	}
}
