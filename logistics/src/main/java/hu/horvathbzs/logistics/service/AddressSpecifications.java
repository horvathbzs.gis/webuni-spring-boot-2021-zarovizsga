
package hu.horvathbzs.logistics.service;

import org.springframework.data.jpa.domain.Specification;

import hu.horvathbzs.logistics.model.Address;
import hu.horvathbzs.logistics.model.Address_;

/**
 * Specifications for Address for search.<br />
 * Search terms:<br />
 * total match: 2 digit country ISO code, ZIP code<br />
 * partial match: municipality, street.
 * 
 * @author balazs.horvath
 *
 */

public class AddressSpecifications {

	/**
	 * Address 2 digit ISO code must match.
	 * 
	 * @param country2DigitIsoCode
	 * @return
	 */
	public static Specification<Address> hasCountry2DigitIsoCode(String country2DigitIsoCode) {
		return (root, cq, cb) -> cb.equal(root.get(Address_.country2DigitIsoCode), country2DigitIsoCode);
	}

	/**
	 * Address municipality matches with the beginning, case-insensitive.
	 * 
	 * @param municipality
	 * @return
	 */
	public static Specification<Address> hasMunicipality(String municipality) {
		return (root, cq, cb) -> cb.like(cb.lower(root.get(Address_.municipality)), (municipality + "%").toLowerCase());
	}

	/**
	 * Address ZIP code must match.
	 * 
	 * @param zipCode
	 * @return
	 */
	public static Specification<Address> hasZipCode(String zipCode) {
		return (root, cq, cb) -> cb.equal(root.get(Address_.zipCode), zipCode);
	}

	/**
	 * Address street matches with the beginning, case-insensitive.
	 * 
	 * @param street
	 * @return
	 */
	public static Specification<Address> hasStreet(String street) {
		return (root, cq, cb) -> cb.like(cb.lower(root.get(Address_.street)), (street + "%").toLowerCase());
	}
}
