package hu.horvathbzs.logistics.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import hu.horvathbzs.logistics.model.Address;
import hu.horvathbzs.logistics.repository.AddressRepository;

/**
 * Service class for Address.<br />
 * CRUD methods.<br />
 * Find by example method.
 * 
 * @author balazs.horvath
 *
 */

@Service
public class AddressService {

	@Autowired
	AddressRepository addressRepository;

	/**
	 * Queries all of the addresses.
	 * 
	 * @return List<Address> all addresses.
	 */
	public List<Address> findAll() {
		return addressRepository.findAll();
	}
	
	/**
	 * Queries all of the addresses with pageable.
	 * 
	 * @return Page<Address> all addresses.
	 */
	public Page<Address> findAll(Pageable pageable) {
		return addressRepository.findAll(pageable);
	}

	/**
	 * Queries one address.
	 * 
	 * @return Optional<Address> if given id exists, return with one address.
	 */
	public Optional<Address> findById(Long id) {
		return addressRepository.findById(id);
	}

	/**
	 * Creates an address record in the database.
	 * 
	 * @param address
	 * @return saved address with generated id.
	 */
	public Address save(Address address) {
		return addressRepository.save(address);
	}

	/**
	 * Updates an existing address record.
	 * 
	 * @param address
	 * @return updated address with existing id.
	 */
	@Transactional
	public Address update(Address address) {
		if (!addressRepository.existsById(address.getId()))
			return null;
		return addressRepository.save(address);
	}

	/**
	 * Deletes an address record.
	 * 
	 * @param id
	 */
	public void delete(Long id) {
		addressRepository.deleteById(id);
	}

	/**
	 * Find all the addresses that match the searching terms.
	 * @param example an address with argument values.
	 * @param pageable
	 * @return List of addresses with arguments that match the searching terms.
	 */
	public Page<Address> findAddressesByExample(Address example, Pageable pageable) {

		String country2DigitIsoCode = example.getCountry2DigitIsoCode();
		String municipality = example.getMunicipality();
		String zipCode = example.getZipCode();
		String street = example.getStreet();
		
		Specification<Address> spec = Specification.where(null);
		if(StringUtils.hasText(country2DigitIsoCode))
			spec = spec.and(AddressSpecifications.hasCountry2DigitIsoCode(country2DigitIsoCode));
		if(StringUtils.hasText(municipality))
			spec = spec.and(AddressSpecifications.hasMunicipality(municipality));
		if(StringUtils.hasText(zipCode))
			spec = spec.and(AddressSpecifications.hasZipCode(zipCode));
		if(StringUtils.hasText(street))
			spec = spec.and(AddressSpecifications.hasStreet(street));
		
		return addressRepository.findAll(spec, pageable);
	}

}
