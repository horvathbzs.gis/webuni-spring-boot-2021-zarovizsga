package hu.horvathbzs.logistics.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import hu.horvathbzs.logistics.model.TransportPlan;


/**
 * Repository interface for Transport Plan.
 * 
 * @author balazs.horvath
 *
 */
public interface TransportPlanRepository extends JpaRepository<TransportPlan, Long>, JpaSpecificationExecutor<TransportPlan> {

	@EntityGraph("TransportPlan.full")
	@Query("SELECT t FROM TransportPlan t")
	public List<TransportPlan> findAllFull();
	
	@EntityGraph("TransportPlan.full")
	public Optional<TransportPlan> findWithSectionsById(Long id);
	
}
