package hu.horvathbzs.logistics.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import hu.horvathbzs.logistics.model.Milestone;
import hu.horvathbzs.logistics.model.Section;
import hu.horvathbzs.logistics.model.TransportPlan;

/**
 * Repository interface for Section.
 * 
 * @author balazs.horvath
 *
 */
public interface SectionRepository extends JpaRepository<Section, Long>, JpaSpecificationExecutor<Section> {

	List<Section> findByTransportPlan(TransportPlan transportPlan);
	
	Optional<Section> findByTransportPlanAndSectionIndex(TransportPlan transportPlan, Integer sectionIndex);

	/**
	 * The section where the given milestone belongs to. Either it is a from or a to
	 * milestone.
	 * 
	 * @param fromMilestone
	 * @param toMilestone
	 * @return a section that contains the given milestone.
	 */
	Optional<Section> findByFromMilestoneOrToMilestone(Milestone fromMilestone, Milestone toMilestone);
}
