package hu.horvathbzs.logistics.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import hu.horvathbzs.logistics.model.Milestone;


/**
 * Repository interface for Milestone.
 * 
 * @author balazs.horvath
 *
 */
public interface MilestoneRepository extends JpaRepository<Milestone, Long>, JpaSpecificationExecutor<Milestone> {

}
