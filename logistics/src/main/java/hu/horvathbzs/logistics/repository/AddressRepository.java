package hu.horvathbzs.logistics.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import hu.horvathbzs.logistics.model.Address;

/**
 * Repository interface for Address.
 * 
 * @author balazs.horvath
 *
 */
public interface AddressRepository extends JpaRepository<Address, Long>, JpaSpecificationExecutor<Address>,
		PagingAndSortingRepository<Address, Long> {

}
