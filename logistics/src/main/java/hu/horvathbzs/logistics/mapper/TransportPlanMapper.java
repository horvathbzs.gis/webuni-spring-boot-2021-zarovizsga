package hu.horvathbzs.logistics.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import hu.horvathbzs.logistics.dto.AddressDto;
import hu.horvathbzs.logistics.dto.MilestoneDto;
import hu.horvathbzs.logistics.dto.SectionDto;
import hu.horvathbzs.logistics.dto.TransportPlanDto;
import hu.horvathbzs.logistics.model.Address;
import hu.horvathbzs.logistics.model.Milestone;
import hu.horvathbzs.logistics.model.Section;
import hu.horvathbzs.logistics.model.TransportPlan;

/**
 * Mapper interface for Transport Plan.
 * 
 * @author balazs.horvath
 *
 */

@Mapper(componentModel = "spring")
public interface TransportPlanMapper {

	TransportPlan dtoToTransportPlan(TransportPlanDto transportPlanDto);

	TransportPlanDto transportPlanToDto(TransportPlan transportPlan);

	List<TransportPlan> dtosToTransportPlans(List<TransportPlanDto> dtos);

	List<TransportPlanDto> transportPlansToDtos(List<TransportPlan> transportPlans);

	@Mapping(source = "transportPlan.id", target = "transportPlanId")
	@Mapping(source = "fromMilestone.id", target = "fromMilestoneId")
	@Mapping(source = "toMilestone.id", target = "toMilestoneId")
	SectionDto sectionToDto(Section section);
	
	@Mapping(source = "address.id", target = "addressId")
	MilestoneDto milestoneToDto(Milestone milestone);
	
	AddressDto addressToDto(Address address);
}
