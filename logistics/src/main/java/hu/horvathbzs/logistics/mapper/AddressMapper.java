package hu.horvathbzs.logistics.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import hu.horvathbzs.logistics.dto.AddressDto;
import hu.horvathbzs.logistics.model.Address;

/**
 * Mapper interface for Address.
 * 
 * @author balazs.horvath
 *
 */

@Mapper(componentModel = "spring")
public interface AddressMapper {

	Address dtoToAddress(AddressDto addressDto);

	AddressDto addressToDto(Address address);

	List<Address> dtosToAddresses(List<AddressDto> dtos);

	List<AddressDto> addressesToDtos(List<Address> addresses);
}
