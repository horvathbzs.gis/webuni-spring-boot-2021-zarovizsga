package hu.horvathbzs.logistics.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import hu.horvathbzs.logistics.dto.MilestoneDto;
import hu.horvathbzs.logistics.model.Milestone;

/**
 * Mapper interface for Milestone.
 * 
 * @author balazs.horvath
 *
 */

@Mapper(componentModel = "spring")
public interface MilestoneMapper {

	@Mapping(source = "addressId", target = "address.id")
	Milestone dtoToMilestone(MilestoneDto milestoneDto);

	@Mapping(source = "address.id", target = "addressId")
	MilestoneDto milestoneToDto(Milestone milestone);

	List<Milestone> dtosToMilestones(List<MilestoneDto> dtos);

	List<MilestoneDto> milestoneesToDtos(List<Milestone> milestonees);
}
