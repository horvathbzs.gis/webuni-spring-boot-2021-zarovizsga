package hu.horvathbzs.logistics.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import hu.horvathbzs.logistics.dto.SectionDto;
import hu.horvathbzs.logistics.model.Section;

/**
 * Mapper interface for Section.
 * 
 * @author balazs.horvath
 *
 */

@Mapper(componentModel = "spring")
public interface SectionMapper {

	@Mapping(source = "transportPlanId", target = "transportPlan.id")
	@Mapping(source = "fromMilestoneId", target = "fromMilestone.id")
	@Mapping(source = "toMilestoneId", target = "toMilestone.id")
	Section dtoToSection(SectionDto sectionDto);

	@Mapping(source = "transportPlan.id", target = "transportPlanId")
	@Mapping(source = "fromMilestone.id", target = "fromMilestoneId")
	@Mapping(source = "toMilestone.id", target = "toMilestoneId")
	SectionDto sectionToDto(Section section);

	List<Section> dtosToSections(List<SectionDto> dtos);

	List<SectionDto> sectionsToDtos(List<Section> sections);
}
