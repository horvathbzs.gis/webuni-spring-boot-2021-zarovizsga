package hu.horvathbzs.logistics.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Entity class for Section, a section of a transport plan.<br />
 * Many sections belong to one transport plan.<br />
 * Consist of a starting and an ending milestone.<br />
 * Section index shows that the section is which section in the sequence of sections
 * in a transport plan.<br />
 * A section's toMilestone and the following section's fromMilestone are different.
 * 
 * @author balazs.horvath
 *
 */
@Entity
public class Section {

	@Id
	@GeneratedValue
	private Long id;
	private Integer sectionIndex;
	@ManyToOne
	private TransportPlan transportPlan;
	@ManyToOne
	private Milestone fromMilestone;
	@ManyToOne
	private Milestone toMilestone;

	/**
	 * No Arg Constructor.
	 */
	public Section() {

	}

	/**
	 * All Arg Constructor.
	 * 
	 * @param id
	 * @param sectionIndex
	 * @param transportPlan
	 * @param fromMilestone
	 * @param toMilestone
	 */
	public Section(Long id, Integer sectionIndex, TransportPlan transportPlan, Milestone fromMilestone,
			Milestone toMilestone) {
		this.id = id;
		this.sectionIndex = sectionIndex;
		this.transportPlan = transportPlan;
		this.fromMilestone = fromMilestone;
		this.toMilestone = toMilestone;
	}

	/**
	 * Adds extra minutes to planned time to both from and to milestone.plannedTime.
	 * 
	 * @param delay Long minutes of delay. LocalDateTime.plusMinutes method requires
	 *              long type.
	 */
	public void modifyFromAndToMilestonePlannedTimeWithDelay(Long delay) {
		this.fromMilestone.setPlannedTime(this.fromMilestone.getPlannedTime().plusMinutes(delay));
		this.toMilestone.setPlannedTime(this.toMilestone.getPlannedTime().plusMinutes(delay));
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the sectionIndex
	 */
	public Integer getSectionIndex() {
		return sectionIndex;
	}

	/**
	 * @param sectionIndex the sectionIndex to set
	 */
	public void setSectionIndex(Integer sectionIndex) {
		this.sectionIndex = sectionIndex;
	}

	/**
	 * @return the transportPlan
	 */
	public TransportPlan getTransportPlan() {
		return transportPlan;
	}

	/**
	 * @param transportPlan the transportPlan to set
	 */
	public void setTransportPlan(TransportPlan transportPlan) {
		this.transportPlan = transportPlan;
	}

	/**
	 * @return the fromMilestone
	 */
	public Milestone getFromMilestone() {
		return fromMilestone;
	}

	/**
	 * @param fromMilestone the fromMilestone to set
	 */
	public void setFromMilestone(Milestone fromMilestone) {
		this.fromMilestone = fromMilestone;
	}

	/**
	 * @return the toMilestone
	 */
	public Milestone getToMilestone() {
		return toMilestone;
	}

	/**
	 * @param toMilestone the toMilestone to set
	 */
	public void setToMilestone(Milestone toMilestone) {
		this.toMilestone = toMilestone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromMilestone == null) ? 0 : fromMilestone.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((sectionIndex == null) ? 0 : sectionIndex.hashCode());
		result = prime * result + ((toMilestone == null) ? 0 : toMilestone.hashCode());
		result = prime * result + ((transportPlan == null) ? 0 : transportPlan.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Section)) {
			return false;
		}
		Section other = (Section) obj;
		if (fromMilestone == null) {
			if (other.fromMilestone != null) {
				return false;
			}
		} else if (!fromMilestone.equals(other.fromMilestone)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (sectionIndex == null) {
			if (other.sectionIndex != null) {
				return false;
			}
		} else if (!sectionIndex.equals(other.sectionIndex)) {
			return false;
		}
		if (toMilestone == null) {
			if (other.toMilestone != null) {
				return false;
			}
		} else if (!toMilestone.equals(other.toMilestone)) {
			return false;
		}
		if (transportPlan == null) {
			if (other.transportPlan != null) {
				return false;
			}
		} else if (!transportPlan.equals(other.transportPlan)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Section [");
		if (id != null)
			builder.append("id=").append(id).append(", ");
		if (sectionIndex != null)
			builder.append("sectionIndex=").append(sectionIndex).append(", ");
		if (transportPlan != null)
			builder.append("transportPlan=").append(transportPlan).append(", ");
		if (fromMilestone != null)
			builder.append("fromMilestone=").append(fromMilestone).append(", ");
		if (toMilestone != null)
			builder.append("toMilestone=").append(toMilestone);
		builder.append("]");
		return builder.toString();
	}
}
