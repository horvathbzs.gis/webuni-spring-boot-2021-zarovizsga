
package hu.horvathbzs.logistics.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;

/**
 * Entity class for TransportPlan.<br />
 * A transport plan contains an expected revenue and a list of sections.<br />
 * A section consist of a starting and an ending milestone.<br />
 * A section's toMilestone and the following section's fromMilestone are different.
 * 
 * @author balazs.horvath
 *
 */
@NamedEntityGraph(name = "TransportPlan.full", attributeNodes = { @NamedAttributeNode(value = "sections") } )
@Entity
public class TransportPlan {

	@Id
	@GeneratedValue
	private Long id;
	private Double expectedRevenue;

	@OneToMany(mappedBy = "transportPlan")
	private List<Section> sections;

	/**
	 * No Arg Constructor.
	 */
	public TransportPlan() {
	}

	/**
	 * All Arg Constructor.
	 * 
	 * @param id
	 * @param expectedRevenue
	 * @param sections
	 */
	public TransportPlan(Long id, Double expectedRevenue, List<Section> sections) {
		this.id = id;
		this.expectedRevenue = expectedRevenue;
		this.sections = sections;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the expectedRevenue
	 */
	public Double getExpectedRevenue() {
		return expectedRevenue;
	}

	/**
	 * @param expectedRevenue the expectedRevenue to set
	 */
	public void setExpectedRevenue(Double expectedRevenue) {
		this.expectedRevenue = expectedRevenue;
	}

	/**
	 * @return the sections
	 */
	public List<Section> getSections() {
		return sections;
	}

	/**
	 * @param sections the sections to set
	 */
	public void setSections(List<Section> sections) {
		this.sections = sections;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expectedRevenue == null) ? 0 : expectedRevenue.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((sections == null) ? 0 : sections.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof TransportPlan)) {
			return false;
		}
		TransportPlan other = (TransportPlan) obj;
		if (expectedRevenue == null) {
			if (other.expectedRevenue != null) {
				return false;
			}
		} else if (!expectedRevenue.equals(other.expectedRevenue)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (sections == null) {
			if (other.sections != null) {
				return false;
			}
		} else if (!sections.equals(other.sections)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransportPlan [");
		if (id != null)
			builder.append("id=").append(id).append(", ");
		if (expectedRevenue != null)
			builder.append("expectedRevenue=").append(expectedRevenue).append(", ");
		if (sections != null)
			builder.append("sections=").append(sections);
		builder.append("]");
		return builder.toString();
	}
}
