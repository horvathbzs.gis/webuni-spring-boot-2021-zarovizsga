
package hu.horvathbzs.logistics.dto;

import java.time.LocalDateTime;

/**
 * DTO class for Milestone.
 * 
 * @author balazs.horvath
 *
 */
public class MilestoneDto {

	private Long id;
	private Long addressId;
	private LocalDateTime plannedTime;

	/**
	 * No Arg Constructor.
	 */
	public MilestoneDto() {

	}

	/**
	 * All Arg Constructor.
	 * @param id
	 * @param address
	 * @param plannedTime
	 */
	public MilestoneDto(Long id, Long addressId, LocalDateTime plannedTime) {
		this.id = id;
		this.addressId = addressId;
		this.plannedTime = plannedTime;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the addressId
	 */
	public Long getAddressId() {
		return addressId;
	}

	/**
	 * @param addressId the addressId to set
	 */
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	/**
	 * @return the plannedTime
	 */
	public LocalDateTime getPlannedTime() {
		return plannedTime;
	}

	/**
	 * @param plannedTime the plannedTime to set
	 */
	public void setPlannedTime(LocalDateTime plannedTime) {
		this.plannedTime = plannedTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addressId == null) ? 0 : addressId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((plannedTime == null) ? 0 : plannedTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof MilestoneDto)) {
			return false;
		}
		MilestoneDto other = (MilestoneDto) obj;
		if (addressId == null) {
			if (other.addressId != null) {
				return false;
			}
		} else if (!addressId.equals(other.addressId)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (plannedTime == null) {
			if (other.plannedTime != null) {
				return false;
			}
		} else if (!plannedTime.equals(other.plannedTime)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MilestoneDto [");
		if (id != null)
			builder.append("id=").append(id).append(", ");
		if (addressId != null)
			builder.append("addressId=").append(addressId).append(", ");
		if (plannedTime != null)
			builder.append("plannedTime=").append(plannedTime);
		builder.append("]");
		return builder.toString();
	}
}
