package hu.horvathbzs.logistics.dto;

/**
 * DTO class for Section.
 * 
 * @author balazs.horvath
 *
 */
public class SectionDto {

	private Long id;
	private Integer sectionIndex;
	private Long transportPlanId;
	private Long fromMilestoneId;
	private Long toMilestoneId;

	/**
	 * No Arg Constructor.
	 */
	public SectionDto() {

	}

	/**
	 * All Arg Constructor.
	 * 
	 * @param id
	 * @param sectionIndex
	 * @param transportPlanId
	 * @param fromMilestoneId
	 * @param toMilestoneId
	 */
	public SectionDto(Long id, Integer sectionIndex, Long transportPlanId, Long fromMilestoneId, Long toMilestoneId) {
		this.id = id;
		this.sectionIndex = sectionIndex;
		this.transportPlanId = transportPlanId;
		this.fromMilestoneId = fromMilestoneId;
		this.toMilestoneId = toMilestoneId;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the sectionIndex
	 */
	public Integer getSectionIndex() {
		return sectionIndex;
	}

	/**
	 * @param sectionIndex the sectionIndex to set
	 */
	public void setSectionIndex(Integer sectionIndex) {
		this.sectionIndex = sectionIndex;
	}

	/**
	 * @return the transportPlanId
	 */
	public Long getTransportPlanId() {
		return transportPlanId;
	}

	/**
	 * @param transportPlanId the transportPlanId to set
	 */
	public void setTransportPlanId(Long transportPlanId) {
		this.transportPlanId = transportPlanId;
	}

	/**
	 * @return the fromMilestoneId
	 */
	public Long getFromMilestoneId() {
		return fromMilestoneId;
	}

	/**
	 * @param fromMilestoneId the fromMilestoneId to set
	 */
	public void setFromMilestoneId(Long fromMilestoneId) {
		this.fromMilestoneId = fromMilestoneId;
	}

	/**
	 * @return the toMilestoneId
	 */
	public Long getToMilestoneId() {
		return toMilestoneId;
	}

	/**
	 * @param toMilestoneId the toMilestoneId to set
	 */
	public void setToMilestoneId(Long toMilestoneId) {
		this.toMilestoneId = toMilestoneId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromMilestoneId == null) ? 0 : fromMilestoneId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((sectionIndex == null) ? 0 : sectionIndex.hashCode());
		result = prime * result + ((toMilestoneId == null) ? 0 : toMilestoneId.hashCode());
		result = prime * result + ((transportPlanId == null) ? 0 : transportPlanId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof SectionDto)) {
			return false;
		}
		SectionDto other = (SectionDto) obj;
		if (fromMilestoneId == null) {
			if (other.fromMilestoneId != null) {
				return false;
			}
		} else if (!fromMilestoneId.equals(other.fromMilestoneId)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (sectionIndex == null) {
			if (other.sectionIndex != null) {
				return false;
			}
		} else if (!sectionIndex.equals(other.sectionIndex)) {
			return false;
		}
		if (toMilestoneId == null) {
			if (other.toMilestoneId != null) {
				return false;
			}
		} else if (!toMilestoneId.equals(other.toMilestoneId)) {
			return false;
		}
		if (transportPlanId == null) {
			if (other.transportPlanId != null) {
				return false;
			}
		} else if (!transportPlanId.equals(other.transportPlanId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SectionDto [");
		if (id != null)
			builder.append("id=").append(id).append(", ");
		if (sectionIndex != null)
			builder.append("sectionIndex=").append(sectionIndex).append(", ");
		if (transportPlanId != null)
			builder.append("transportPlanId=").append(transportPlanId).append(", ");
		if (fromMilestoneId != null)
			builder.append("fromMilestoneId=").append(fromMilestoneId).append(", ");
		if (toMilestoneId != null)
			builder.append("toMilestoneId=").append(toMilestoneId);
		builder.append("]");
		return builder.toString();
	}
}
