
package hu.horvathbzs.logistics.dto;

import java.util.List;

/**
 * DTO class for Transport Plan.
 * 
 * @author balazs.horvath
 *
 */
public class TransportPlanDto {

	private Long id;
	private Double expectedRevenue;
	private List<SectionDto> sections;

	/**
	 * No Arg Constructor.
	 */
	public TransportPlanDto() {

	}

	/**
	 * All Arg Constructor.
	 * 
	 * @param id
	 * @param expectedRevenue
	 * @param sections        List<SectionDto>
	 */
	public TransportPlanDto(Long id, Double expectedRevenue, List<SectionDto> sections) {
		this.id = id;
		this.expectedRevenue = expectedRevenue;
		this.sections = sections;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the expectedRevenue
	 */
	public Double getExpectedRevenue() {
		return expectedRevenue;
	}

	/**
	 * @param expectedRevenue the expectedRevenue to set
	 */
	public void setExpectedRevenue(Double expectedRevenue) {
		this.expectedRevenue = expectedRevenue;
	}

	/**
	 * @return the sections
	 */
	public List<SectionDto> getSections() {
		return sections;
	}

	/**
	 * @param sections the sections to set
	 */
	public void setSections(List<SectionDto> sections) {
		this.sections = sections;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expectedRevenue == null) ? 0 : expectedRevenue.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((sections == null) ? 0 : sections.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof TransportPlanDto)) {
			return false;
		}
		TransportPlanDto other = (TransportPlanDto) obj;
		if (expectedRevenue == null) {
			if (other.expectedRevenue != null) {
				return false;
			}
		} else if (!expectedRevenue.equals(other.expectedRevenue)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (sections == null) {
			if (other.sections != null) {
				return false;
			}
		} else if (!sections.equals(other.sections)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		final int maxLen = 2;
		StringBuilder builder = new StringBuilder();
		builder.append("TrasnportPlanDto [");
		if (id != null)
			builder.append("id=").append(id).append(", ");
		if (expectedRevenue != null)
			builder.append("expectedRevenue=").append(expectedRevenue).append(", ");
		if (sections != null)
			builder.append("sections=").append(sections.subList(0, Math.min(sections.size(), maxLen)));
		builder.append("]");
		return builder.toString();
	}
}