package hu.horvathbzs.logistics.dto;

import javax.validation.constraints.NotBlank;

/**
 * DTO class for Address.<br />
 * Country 2 digit ISO code, municipality, street name, house number, ZIP code
 * must not be blank.
 * 
 * @author balazs.horvath
 * 
 */
public class AddressDto {

	private Long id;
	@NotBlank
	private String country2DigitIsoCode;
	@NotBlank
	private String municipality;
	@NotBlank
	private String street;
	@NotBlank
	private String houseNumber;
	@NotBlank
	private String zipCode;
	private Double geoLatitude;
	private Double geoLongitude;

	/**
	 * No Arg Constructor.
	 */
	public AddressDto() {

	}

	/**
	 * All Arg Constructor.
	 * 
	 * @param id
	 * @param country2DigitIsoCode
	 * @param municipality
	 * @param street
	 * @param houseNumber
	 * @param zipCode
	 * @param geoLatitude
	 * @param geoLongitude
	 */
	public AddressDto(Long id, @NotBlank String country2DigitIsoCode, @NotBlank String municipality,
			@NotBlank String street, @NotBlank String houseNumber, @NotBlank String zipCode, Double geoLatitude,
			Double geoLongitude) {
		this.id = id;
		this.country2DigitIsoCode = country2DigitIsoCode;
		this.municipality = municipality;
		this.street = street;
		this.houseNumber = houseNumber;
		this.zipCode = zipCode;
		this.geoLatitude = geoLatitude;
		this.geoLongitude = geoLongitude;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the country2DigitIsoCode
	 */
	public String getCountry2DigitIsoCode() {
		return country2DigitIsoCode;
	}

	/**
	 * @param country2DigitIsoCode the country2DigitIsoCode to set
	 */
	public void setCountry2DigitIsoCode(String country2DigitIsoCode) {
		this.country2DigitIsoCode = country2DigitIsoCode;
	}

	/**
	 * @return the municipality
	 */
	public String getMunicipality() {
		return municipality;
	}

	/**
	 * @param municipality the municipality to set
	 */
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the houseNumber
	 */
	public String getHouseNumber() {
		return houseNumber;
	}

	/**
	 * @param houseNumber the houseNumber to set
	 */
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the geoLatitude
	 */
	public Double getGeoLatitude() {
		return geoLatitude;
	}

	/**
	 * @param geoLatitude the geoLatitude to set
	 */
	public void setGeoLatitude(Double geoLatitude) {
		this.geoLatitude = geoLatitude;
	}

	/**
	 * @return the geoLongitude
	 */
	public Double getGeoLongitude() {
		return geoLongitude;
	}

	/**
	 * @param geoLongitude the geoLongitude to set
	 */
	public void setGeoLongitude(Double geoLongitude) {
		this.geoLongitude = geoLongitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country2DigitIsoCode == null) ? 0 : country2DigitIsoCode.hashCode());
		result = prime * result + ((geoLatitude == null) ? 0 : geoLatitude.hashCode());
		result = prime * result + ((geoLongitude == null) ? 0 : geoLongitude.hashCode());
		result = prime * result + ((houseNumber == null) ? 0 : houseNumber.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((municipality == null) ? 0 : municipality.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof AddressDto)) {
			return false;
		}
		AddressDto other = (AddressDto) obj;
		if (country2DigitIsoCode == null) {
			if (other.country2DigitIsoCode != null) {
				return false;
			}
		} else if (!country2DigitIsoCode.equals(other.country2DigitIsoCode)) {
			return false;
		}
		if (geoLatitude == null) {
			if (other.geoLatitude != null) {
				return false;
			}
		} else if (!geoLatitude.equals(other.geoLatitude)) {
			return false;
		}
		if (geoLongitude == null) {
			if (other.geoLongitude != null) {
				return false;
			}
		} else if (!geoLongitude.equals(other.geoLongitude)) {
			return false;
		}
		if (houseNumber == null) {
			if (other.houseNumber != null) {
				return false;
			}
		} else if (!houseNumber.equals(other.houseNumber)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (municipality == null) {
			if (other.municipality != null) {
				return false;
			}
		} else if (!municipality.equals(other.municipality)) {
			return false;
		}
		if (street == null) {
			if (other.street != null) {
				return false;
			}
		} else if (!street.equals(other.street)) {
			return false;
		}
		if (zipCode == null) {
			if (other.zipCode != null) {
				return false;
			}
		} else if (!zipCode.equals(other.zipCode)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AdressDto [");
		if (id != null)
			builder.append("id=").append(id).append(", ");
		if (country2DigitIsoCode != null)
			builder.append("country2DigitIsoCode=").append(country2DigitIsoCode).append(", ");
		if (municipality != null)
			builder.append("municipality=").append(municipality).append(", ");
		if (street != null)
			builder.append("street=").append(street).append(", ");
		if (houseNumber != null)
			builder.append("houseNumber=").append(houseNumber).append(", ");
		if (zipCode != null)
			builder.append("zipCode=").append(zipCode).append(", ");
		if (geoLatitude != null)
			builder.append("geoLatitude=").append(geoLatitude).append(", ");
		if (geoLongitude != null)
			builder.append("geoLongitude=").append(geoLongitude);
		builder.append("]");
		return builder.toString();
	}
}
