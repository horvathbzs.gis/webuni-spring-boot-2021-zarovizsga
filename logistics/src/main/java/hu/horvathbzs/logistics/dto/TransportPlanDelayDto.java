
package hu.horvathbzs.logistics.dto;

/**
 * DTO class to store the delay of a milestone.
 * 
 * @author balazs.horvath
 *
 */
public class TransportPlanDelayDto {

	private Long milestoneId;
	private Long delay;

	/**
	 * No Arg Constructor.
	 */
	public TransportPlanDelayDto() {

	}

	/**
	 * All Arg Constructor.
	 * 
	 * @param milestoneId
	 * @param delay
	 */
	public TransportPlanDelayDto(Long milestoneId, Long delay) {
		this.milestoneId = milestoneId;
		this.delay = delay;
	}

	/**
	 * @return the milestoneId
	 */
	public Long getMilestoneId() {
		return milestoneId;
	}

	/**
	 * @param milestoneId the milestoneId to set
	 */
	public void setMilestoneId(Long milestoneId) {
		this.milestoneId = milestoneId;
	}

	/**
	 * @return the delay
	 */
	public Long getDelay() {
		return delay;
	}

	/**
	 * @param delay the delay to set
	 */
	public void setDelay(Long delay) {
		this.delay = delay;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((delay == null) ? 0 : delay.hashCode());
		result = prime * result + ((milestoneId == null) ? 0 : milestoneId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof TransportPlanDelayDto)) {
			return false;
		}
		TransportPlanDelayDto other = (TransportPlanDelayDto) obj;
		if (delay == null) {
			if (other.delay != null) {
				return false;
			}
		} else if (!delay.equals(other.delay)) {
			return false;
		}
		if (milestoneId == null) {
			if (other.milestoneId != null) {
				return false;
			}
		} else if (!milestoneId.equals(other.milestoneId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransportPlanDelayDto [");
		if (milestoneId != null)
			builder.append("milestoneId=").append(milestoneId).append(", ");
		if (delay != null)
			builder.append("delay=").append(delay);
		builder.append("]");
		return builder.toString();
	}
}
