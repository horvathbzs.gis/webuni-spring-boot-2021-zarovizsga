
package hu.horvathbzs.logistics.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import hu.horvathbzs.logistics.security.JwtAuthFilter;

/**
 * Configuration and Websecurity class, extends from
 * WebSecurityConfigurerAdapter. User credentials from memory.
 * 
 * @author balazs.horvath
 *
 */

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	JwtAuthFilter jwtAuthfilter;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().passwordEncoder(passwordEncoder()).withUser("userAddressManager")
				.authorities("addressmanager").password(passwordEncoder().encode("pass")).and()
				.withUser("userTransportManager").authorities("transportmanager")
				.password(passwordEncoder().encode("pass"));
	}

	/**
	 * Stateless session creation policy. JWT token authorization. JwtAuthFilter
	 * parses jwt token back to credentials.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().and().csrf().disable().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
				.antMatchers(HttpMethod.POST, "/api/addresses/**").hasAuthority("addressmanager")
				.antMatchers(HttpMethod.PUT, "/api/addresses/**").hasAuthority("addressmanager")
				.antMatchers(HttpMethod.DELETE, "/api/addresses/**").hasAuthority("addressmanager")
				.antMatchers(HttpMethod.POST, "/api/transportPlans/{id}/delay").hasAuthority("transportmanager")
				.anyRequest().permitAll();

		http.addFilterBefore(jwtAuthfilter, UsernamePasswordAuthenticationFilter.class);

	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
}
