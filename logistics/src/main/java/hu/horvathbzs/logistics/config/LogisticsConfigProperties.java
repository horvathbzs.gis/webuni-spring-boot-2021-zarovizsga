package hu.horvathbzs.logistics.config;

import java.util.TreeMap;
import java.time.Duration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Configuration Properties class.
 * 
 * @author balazs.horvath
 *
 */
@ConfigurationProperties(prefix = "logistics")
@Component
public class LogisticsConfigProperties {

	private ExpectedRevenueReduceByDelay expectedRevenueReduceByDelay = new ExpectedRevenueReduceByDelay();
	private JwtData jwt = new JwtData();

	/**
	 * @return the expectedRevenueReduceByDelay
	 */
	public ExpectedRevenueReduceByDelay getExpectedRevenueReduceByDelay() {
		return expectedRevenueReduceByDelay;
	}

	/**
	 * @param expectedRevenueReduceByDelay the expectedRevenueReduceByDelay to set
	 */
	public void setExpectedRevenueReduceByDelay(ExpectedRevenueReduceByDelay expectedRevenueReduceByDelay) {
		this.expectedRevenueReduceByDelay = expectedRevenueReduceByDelay;
	}

	/**
	 * @return the jwt
	 */
	public JwtData getJwt() {
		return jwt;
	}

	/**
	 * @param jwt the jwt to set
	 */
	public void setJwt(JwtData jwt) {
		this.jwt = jwt;
	}

	public static class ExpectedRevenueReduceByDelay {

		/**
		 * Key: delay minutes, Value: percent of transport plan's expectedRevenue reduce
		 */
		private TreeMap<Long, Double> limits;

		public TreeMap<Long, Double> getLimits() {
			return limits;
		}

		public void setLimits(TreeMap<Long, Double> limits) {
			this.limits = limits;
		}
	}

	public static class JwtData {

		private String issuer;
		private String secret;
		private String alg;
		private Duration duration;

		public String getIssuer() {
			return issuer;
		}

		public void setIssuer(String issuer) {
			this.issuer = issuer;
		}

		public String getSecret() {
			return secret;
		}

		public void setSecret(String secret) {
			this.secret = secret;
		}

		public String getAlg() {
			return alg;
		}

		public void setAlg(String alg) {
			this.alg = alg;
		}

		public Duration getDuration() {
			return duration;
		}

		public void setDuration(Duration duration) {
			this.duration = duration;
		}
	}
}
