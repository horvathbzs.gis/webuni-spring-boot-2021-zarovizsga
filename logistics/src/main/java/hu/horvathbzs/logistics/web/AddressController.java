package hu.horvathbzs.logistics.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import hu.horvathbzs.logistics.dto.AddressDto;
import hu.horvathbzs.logistics.mapper.AddressMapper;
import hu.horvathbzs.logistics.model.Address;
import hu.horvathbzs.logistics.service.AddressService;

/**
 * RestController class for Address
 * 
 * @author balazs.horvath
 *
 */

@RestController
@RequestMapping("api/addresses")
public class AddressController {

	@Autowired
	private AddressMapper addressMapper;

	@Autowired
	private AddressService addressService;

	/**
	 * Method returns with the List of Addresses.<br />
	 * Returns with an empty list if no Addresses in the database.
	 * 
	 * @return List<AddressDto>
	 */
	@GetMapping
	public List<AddressDto> getAll() {
		return addressMapper.addressesToDtos(addressService.findAll());
	}

	/**
	 * Method returns with the Address with the given id, if it exists.
	 * 
	 * @param id of an Address
	 * @return AdressDto, or Response Status Not Found
	 */
	@GetMapping("/{id}")
	public AddressDto getAddress(@PathVariable Long id) {
		Address address = addressService.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		return addressMapper.addressToDto(address);
	}

	/**
	 * Method saves an address in the database and returns with an address with the
	 * saved record id.<br />
	 * Request body cannot contain id property.<br />
	 * Request body is validated.<br />
	 * Country 2 digit ISO code, municipality, street name, house number, ZIP code
	 * must not be blank.
	 * 
	 * @param addressDto a valid Address JSON in the request body
	 * @return AddressDto that contains the saved record's id. Or returns with Bad
	 *         Request(400) response.
	 */
	@PostMapping
	public ResponseEntity<AddressDto> saveAddress(@Valid @RequestBody AddressDto addressDto) {
		if (addressDto.getId() != null) {
			return ResponseEntity.badRequest().build();
		}

		Address address = addressMapper.dtoToAddress(addressDto);
		Address savedAddress = addressService.save(address);
		return ResponseEntity.ok(addressMapper.addressToDto(savedAddress));
	}

	/**
	 * Method modifies an address with the given id in the path and address JSON in
	 * the request body.
	 * 
	 * @param id         id of an Address
	 * @param addressDto a valid Address JSON in the request body
	 * @return Address with the updated data.<br />
	 *         Not Found(404), if the id given in the path does not exist.<br />
	 *         Bad Request(400), if the address JSON is not valid or empty.<br />
	 *         Bad Request(400), if JSON has id property, not equals with path
	 *         variable id.
	 */
	@PutMapping("/{id}")
	public ResponseEntity<AddressDto> modifyAddress(@PathVariable Long id, @Valid @RequestBody AddressDto addressDto) {

		if (addressDto.getId() != null && addressDto.getId() != id) {
			return ResponseEntity.badRequest().build();
		}

		addressDto.setId(id);
		Address address = addressMapper.dtoToAddress(addressDto);
		Address updatedAddress = addressService.update(address);
		if (updatedAddress == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(addressMapper.addressToDto(updatedAddress));
	}

	/**
	 * Deletes the Address from the database with the given id.<br />
	 * Response status 200 OK even if the id in the path variable does not exist.
	 * 
	 * @param id of an Address
	 */
	@DeleteMapping("/{id}")
	public void deleteAddress(@PathVariable Long id) {
		try {
			addressService.delete(id);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.OK);
		}
	}

	/**
	 * Method returns with a response status 200 OK, the response body is a list of
	 * addresses that match the example address JSON serching terms. The response
	 * header contains the number of the addresses of the page.
	 * 
	 * @param addressDto Address JSON in the request body, an example of serching
	 *                   terms
	 * @param page       0 if no value added
	 * @param size       Integer.MAX_VALUE if no value added
	 * @param sort       default value: id, default sort direction: ascending, if
	 *                   parameter value contains [comma] + desc, then direction is
	 *                   descending, if something else than the text desc follows
	 *                   the comma, direction remains ascending
	 * @return HTTP status 200 OK, header the size of the addresses query result,
	 *         body list of addresses
	 */
	@PostMapping("/search")
	public ResponseEntity<List<AddressDto>> searchAddress(@RequestBody AddressDto addressDto,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size,
			@RequestParam(required = false) String sort) {

		/**
		 * Setting PageRequest parameters.
		 */
		Direction direction = Direction.ASC;
		String[] sortAndDirection = null;

		if (null == page)
			page = 0;

		if (null == size)
			size = Integer.MAX_VALUE;

		if (null == sort) {
			sort = "id";
		} else {
			sortAndDirection = sort.split(",");
			sort = sortAndDirection[0];
			if (sortAndDirection.length > 1 && sortAndDirection[1].equals("desc")) {
				direction = Direction.DESC;
			}
		}

		/**
		 * Querying the Addresses with the given paging and sorting parameters.
		 */
		Pageable pageable = PageRequest.of(page, size, Sort.by(direction, sort));
		Page<Address> foundAddresses = addressService.findAddressesByExample(addressMapper.dtoToAddress(addressDto),
				pageable);

		/**
		 * Response header contains the number of addresses by the query result.
		 */
		return ResponseEntity.ok().header("X-Total-Value", String.valueOf(foundAddresses.getContent().size()))
				.body(addressMapper.addressesToDtos(foundAddresses.getContent()));
	}
}
