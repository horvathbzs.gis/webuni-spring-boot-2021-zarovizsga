package hu.horvathbzs.logistics.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import hu.horvathbzs.logistics.dto.TransportPlanDelayDto;
import hu.horvathbzs.logistics.dto.TransportPlanDto;
import hu.horvathbzs.logistics.mapper.TransportPlanMapper;
import hu.horvathbzs.logistics.model.TransportPlan;
import hu.horvathbzs.logistics.service.TransportPlanService;

/**
 * RestController class for TransportPlan
 * 
 * @author balazs.horvath
 *
 */

@RestController
@RequestMapping("api/transportPlans")
public class TransportPlanController {

	@Autowired
	private TransportPlanMapper transportPlanMapper;

	@Autowired
	private TransportPlanService transportPlanService;

	/**
	 * Method returns with the List of TransportPlans.<br />
	 * Returns with an empty list if no TransportPlans in the database.
	 * 
	 * @return List<TransportPlanDto>
	 */
	@GetMapping
	public List<TransportPlanDto> getAll() {
		return transportPlanMapper.transportPlansToDtos(transportPlanService.findAllFull());
	}

	/**
	 * Method returns with the TransportPlan with the given id, if it exists.
	 * 
	 * @param id of a TransportPlan
	 * @return TransportPlanDto, or Response Status Not Found
	 */
	@GetMapping("/{id}")
	public TransportPlanDto getTransportPlan(@PathVariable Long id) {
		TransportPlan transportPlan = transportPlanService.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		return transportPlanMapper.transportPlanToDto(transportPlan);
	}

	/**
	 * Method saves a transportPlan in the database and returns with an
	 * transportPlan with the saved record id.<br />
	 * Request body is validated.<br />
	 * Country 2 digit ISO code, municipality, street name, house number, ZIP code
	 * must not be blank.
	 * 
	 * @param transportPlanDto a valid TransportPlan JSON in the request body
	 * @return TransportPlanDto that contains the saved record's id. Or returns with
	 *         Bad Request(400) response.
	 */
	@PostMapping
	public ResponseEntity<TransportPlanDto> saveTransportPlan(@RequestBody TransportPlanDto transportPlanDto) {
		TransportPlan transportPlan = transportPlanMapper.dtoToTransportPlan(transportPlanDto);
		TransportPlan savedTransportPlan = transportPlanService.save(transportPlan);
		return ResponseEntity.ok(transportPlanMapper.transportPlanToDto(savedTransportPlan));
	}

	/**
	 * Method modifies a transportPlan with the given id in the path and
	 * transportPlan JSON in the request body.
	 * 
	 * @param id               id of a TransportPlan
	 * @param transportPlanDto a valid TransportPlan JSON in the request body
	 * @return TransportPlan with the updated data.<br />
	 *         Not Found(404), if the id given in the path does not exist.<br />
	 *         Bad Request(400), if the transportPlan JSON is not valid.
	 */
	@PutMapping("/{id}")
	public ResponseEntity<TransportPlanDto> modifyTransportPlan(@PathVariable Long id,
			@Valid @RequestBody TransportPlanDto transportPlanDto) {
		transportPlanDto.setId(id);
		TransportPlan transportPlan = transportPlanMapper.dtoToTransportPlan(transportPlanDto);
		TransportPlan updatedTransportPlan = transportPlanService.update(transportPlan);
		if (updatedTransportPlan == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(transportPlanMapper.transportPlanToDto(updatedTransportPlan));
	}

	/**
	 * Deletes the TransportPlan from the database with the given id.<br />
	 * Response status 200 OK even if the id in the path variable does not exist.
	 * 
	 * @param id of an TransportPlan
	 */
	@DeleteMapping("/{id}")
	public void deleteTransportPlan(@PathVariable Long id) {
		try {
			transportPlanService.delete(id);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.OK);
		}
	}

	/**
	 * Registers a delay on the given Transport Plan.<br />
	 * A Transport Plan has a list of sections. A section consists of a starter and
	 * a final milestone. This endpoint modifies the planned time of a Milestone in
	 * the Transport Plan.<br />
	 * Further info: see TransportPlanService.registerDelay method's documentation.
	 * 
	 * @param id                    id of a TransportPlan
	 * @param transportPlanDelayDto JSON {"milestoneId" : "1", "delay" : "3"}
	 * @return TransportPlan with the modified expectedRevenue and milestone data.<br />
	 *         Not Found(404), if the TransportPlan id given in the path does not
	 *         exist.<br />
	 *         Bad Request(400), if the Milestone id given in the request param does
	 *         not exist or does not belong to the Transport Plan.
	 */
	@PostMapping("/{id}/delay")
	public ResponseEntity<TransportPlanDto> delay(@PathVariable Long id,
			@RequestBody TransportPlanDelayDto transportPlanDelayDto) {

		Long milestoneId = transportPlanDelayDto.getMilestoneId();
		Long delay = transportPlanDelayDto.getDelay();
		
		TransportPlan transportPlan = transportPlanService.registerDelay(id, milestoneId, delay);
		return ResponseEntity.ok(transportPlanMapper.transportPlanToDto(transportPlan));
	}
}
